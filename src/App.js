import React, { Component } from 'react';
import { connect } from 'react-redux';

import Memo from './containers/MemoContainer'
import memoActions from './actions/memoActions'

import Header from './components/Header/Header'
import './main.bundle.css';

// Maps the state props to the containers props
const mapStateToProps = (state) => (
  {
    memos: state.data
  }
);
//Maps dispatch to props so we can call this.method in container
const mapDispatchToProps = (dispatch) => (
  {
    fetchMemos: (id) => dispatch(memoActions.fetchMemos(id))
  }
);

class App extends Component {
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this.props.fetchMemos()
  }
  renderMemoList() {
    return this.props.memos.map(this.renderMemo)
  }
  renderMemo(data, index) {
    return <Memo key={index} data={data}/>
  }
  render() {
    let app = 'No memos!';
    if (this.props.memos) {
      app = (
        <div className="App">
          <Header />
          <main className="container">
            {this.renderMemoList()}
          </main>
        </div>
      );
    }
    return app
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);