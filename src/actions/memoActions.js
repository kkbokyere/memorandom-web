import Axios from 'axios';
import config from '../config/development.json'

export const FETCH_MEMO_SUCCESS = 'FETCH_MEMO_SUCCESS';
export const FETCH_MEMO_FAILURE = 'FETCH_MEMO_FAILURE';

export const CREATE_MEMO_SUCCESS = 'CREATE_MEMO_SUCCESS';
export const CREATE_MEMO_FAILURE = 'CREATE_MEMO_FAILURE';

export const UPDATE_MEMO_SUCCESS = 'UPDATE_MEMO_SUCCESS';
export const UPDATE_MEMO_FAILURE = 'UPDATE_MEMO_FAILURE';

export const DELETE_MEMO_SUCCESS = 'DELETE_MEMO_SUCCESS';
export const DELETE_MEMO_FAILURE = 'DELETE_MEMO_SUCCESS';

export const updateMemoSuccess = (memo) => {
  return {
    type: UPDATE_MEMO_SUCCESS,
    payload: memo
  }
};

export const updateMemoFailure = (memo) => (
  {
    type: UPDATE_MEMO_FAILURE,
    payload: memo
  }
);

export const deleteMemoSuccess = (id) => (
  {
    type: DELETE_MEMO_SUCCESS,
    payload: id
  }
);

export const deleteMemoFailure = (id) => (
  {
    type: DELETE_MEMO_FAILURE,
    payload: id
  }
);

export const createMemoSuccess = (memo) => {
  return {
    type: CREATE_MEMO_SUCCESS,
    payload: memo
  }
};

export const createMemoFailure = (error) => (
  {
    type: CREATE_MEMO_FAILURE,
    payload: error
  }
);

export const fetchMemosSuccess = (memos) => (
  {
    type: FETCH_MEMO_SUCCESS,
    payload: memos
  }
);

export const fetchMemosFailure = (memos) => (
  {
    type: FETCH_MEMO_FAILURE,
    payload: memos
  }
);

/**
 * Fetch Memos Async
 * /GET /ideas
 * @returns {function(*)}
 */
export const fetchMemos = (id = '') => {
  return (dispatch) => {
    return Axios.get(`${config.endpoint}/ideas/${id}`)
      .then(response => dispatch(fetchMemosSuccess(response.data)))
      .catch(error =>  {
        dispatch(fetchMemosFailure(error));
        throw(error)
      })
  }
};

/**
 * Create Memo Async
 * /POST /ideas/new route to create new memo
 * @returns {function(*)}
 */
export const createMemo = (memo = '') => {
  return (dispatch) => {
    return Axios.post(`${config.endpoint}/ideas/new`)
      .then(response => dispatch(() => {
        return createMemoSuccess(response.data)
      }))
      .catch(error =>  {
        dispatch(createMemoFailure(error));
        throw(error);

      })
  }
};

/**
 * Update Memo Async
 * /PUT /ideas/update/:id route to update new memo
 * @returns {function(*)}
 */
export const updateMemo = (id, memo) => {
  return (dispatch) => {
    return Axios.put(`${config.endpoint}/idea/update/${id}`, memo)
      .then(response => dispatch(function(response) {
        return updateMemoSuccess(response.data)
      }))
      .catch(error =>  {
        dispatch(updateMemoFailure(error));
        throw(error);

      })
  }
};

/**
 * Delete Memo Async
 * /DELETE /ideas/update/:id route to update new memo
 * @returns {function(*)}
 */
export const deleteMemo = (id) => {
  return (dispatch) => {
    return Axios.delete(`${config.endpoint}/idea/delete/${id ? id : ''}`)
      .then(response => dispatch(deleteMemoSuccess(response.data)))
      .catch(error =>  {
        dispatch(deleteMemoFailure(error));
        throw(error);

      })
  }
};

export default {createMemo, updateMemo, deleteMemo, fetchMemos}