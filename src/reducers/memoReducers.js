import {
  FETCH_MEMO_SUCCESS,
  CREATE_MEMO_SUCCESS,
  UPDATE_MEMO_SUCCESS,
  DELETE_MEMO_SUCCESS,
} from '../actions/memoActions';
const INITIAL_STATE = {
  data: [{
    _id: '',
    title: '',
    created_date: '',
    body: '',
  }]
};
const memoReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_MEMO_SUCCESS:
     return action.payload;
    case CREATE_MEMO_SUCCESS:
      return [...state, Object.assign({}, action.payload)];
    case DELETE_MEMO_SUCCESS:
      return state;
    case UPDATE_MEMO_SUCCESS:
      console.log(action.payload)
      return [...state, Object.assign({}, action.payload)];
    default:
      return state;
  }
};

export default memoReducer;