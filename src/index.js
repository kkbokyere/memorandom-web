import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import * as memoActions from './actions/memoActions';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store from './store';

const root = document.getElementById('root');
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>, root);
registerServiceWorker();