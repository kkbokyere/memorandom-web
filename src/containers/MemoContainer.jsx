import React, { Component } from 'react';
import { connect } from 'react-redux';
import InputTitle from '../components/Memo/InputTitle'
import Body from '../components/Memo/Body'
import DeleteButton from '../components/Memo/DeleteButton';
import memoActions from '../actions/memoActions'

// Maps the state props to the containers props
const mapStateToProps = (state) => (
  {
    memos: state.memos
  }
);

//Maps dispatch to props so we can call this.method in container
const mapDispatchToProps = (dispatch) => (
  {
    createMemo: memo => dispatch(memoActions.createMemo(memo)),
    deleteMemo: id => dispatch(memoActions.deleteMemo(id)),
    updateMemo: (id, memo) => dispatch(memoActions.updateMemo(id, memo)),
    fetchMemos: (id) => dispatch(memoActions.fetchMemos(id))
  }
);

/**
 *
 */
class MemoContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
    }
  }
  componentDidUpdate(prevProps, prevState) {
  }
  handleOnBlurTitle(title) {
    this.props.updateMemo(this.props.data._id, {title: title});
  }
  handleOnBlurBody(body) {
    this.props.updateMemo(this.props.data._id, {body: body});
  }
  handleDeleteMemo (id) {
    const self = this;
    this.props.deleteMemo(id).then(() => {
      self.props.fetchMemos()
    })
  }
  render() {
    const {title, body, _id} = this.props.data;
    return (
      <article className="memo">
        <InputTitle title={title} handleOnBlur={(event) => this.handleOnBlurTitle(event.target.value)}/>
        <Body body={body} handleOnBlur={(event) => this.handleOnBlurBody(event.target.value)}/>
        <DeleteButton handleClickDelete={() => this.handleDeleteMemo(_id)} />
      </article>
    )
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(MemoContainer);
