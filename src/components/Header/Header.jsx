import React  from 'react';
import AddButton from './AddButton';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import memoActions from '../../actions/memoActions'

const mapDispatchToProps = (dispatch) => (
  {
    createMemo: memo => dispatch(memoActions.createMemo(memo)),
    fetchMemos: (id) => dispatch(memoActions.fetchMemos(id)),
  }
);
const mapStateToProps = (state) => ({

});

const handleOnClickAdd = (createMemo, fetchMemos) => {
  createMemo().then(()=> {
    fetchMemos()
  });
};

const Header = (props) => {
  return (
    <header className="header">
      <h1 className="logo">Memorandom</h1>
      <AddButton handleOnClickAdd={() => handleOnClickAdd(props.createMemo, props.fetchMemos)}/>
    </header>
  )
};

Header.PropTypes = {
  handleOnClickAdd: PropTypes.func,
  btnText: PropTypes.string,
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);
