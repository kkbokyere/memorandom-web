import React  from 'react';
import PropTypes from 'prop-types';

const AddButton = (props) => {
  return (
    <button className="memo__add-btn" onClick={props.handleOnClickAdd}>{props.btnText}</button>
  )
};

AddButton.PropTypes = {
  handleOnClickAdd: PropTypes.func,
  btnText: PropTypes.string,
};

AddButton.defaultProps = {
  btnText:'Add New Memo'
};

export default AddButton;