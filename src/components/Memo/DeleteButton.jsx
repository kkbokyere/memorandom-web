import React  from 'react';
import PropTypes from 'prop-types';

const DeleteButton = (props) => {
  return (
    <button className="memo__delete-btn" onClick={props.handleClickDelete}>{props.btnText}</button>
  )
};

DeleteButton.PropTypes = {
  handleClickDelete: PropTypes.func,
  btnText: PropTypes.string
};

DeleteButton.defaultProps = {
  btnText: 'Delete',
};

export default DeleteButton;