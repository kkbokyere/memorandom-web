import React from 'react';
import PropTypes from 'prop-types';

const InputTitle = (props) => {
  return (
    <input autoFocus={!props.title} type="text" value={props.title} className="memo__title" onKeyUp={props.handleOnBlur} placeholder="Title..." ref={(input) => this.input = input}/>
  )
};

InputTitle.PropTypes = {
  title: PropTypes.string,
  handleOnBlur: PropTypes.func
};

export default InputTitle;