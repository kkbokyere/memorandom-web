import React  from 'react';
import PropTypes from 'prop-types';

const Body = (props) => {
  return (
    <textarea className="memo__body" value={props.body} onKeyUp={props.handleOnBlur} placeholder="Enter body..."/>
  )
};

Body.PropTypes = {
  body: PropTypes.string,
  handleOnBlur: PropTypes.func
};

export default Body;