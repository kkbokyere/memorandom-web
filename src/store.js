import { applyMiddleware, createStore } from 'redux';
import { createLogger } from 'redux-logger'; //middleware for logging
import thunk from 'redux-thunk'; // middleware to allow us to do multi dispatches

import reducer from './reducers/memoReducers';

// Middleware after dispatch is called
const middleware = applyMiddleware(
  thunk,
  createLogger()
);
export default createStore(reducer, middleware);