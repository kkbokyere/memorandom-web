# Memorandom Web App.

A simple React App used to add and ideas/memos.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. Currently, no live deployment pipeline has been catered for.

### Prerequisites

In order to get this working, you would need the [Memorandom API](https://bitbucket.org/kkbokyere/memorandom-api) installed on your dev machine.

Once you have these installed and you have checked out this repo, you should be ready to start installation.

### Installing

Run the install script

```
$ yarn install
```

Assuming all dependencies downloaded okay, run the start:dev script and the service should start working!

```
$ yarn run start
```

If its all good, check http://localhost:3001

## Running the tests

Test are built using [Mocha](http://mochajs.org/). you can run them by using the following command:

```
$ yarn run test:dev
```

## Deployment

If it were to be deployed to live, It would use Docker to create a container on the deployment server.

## Built With

* [ReactJS](https://facebook.github.io/react/) - UI component library
* [Redux](http://redux.js.org/) - Flux state management
* [Create React App](https://github.com/facebookincubator/create-react-app) - ReactJS Bootstrap

### Improvements

- Better test coverage
- Server side rendering